require "aws-sdk"

log = Logger.new(STDOUT)
log.level = Logger::INFO


AWS.config(:region => 'ap-southeast-2')

topics = Array.new

#Get the arguments (local testing)
#ARGV.each do|a|
#  topics.push(a)
#end
# Expecting a comma limited string of topics e.g: TopicA,TopicB
topics = (Net::HTTP.get(URI.parse("http://169.254.169.254/1.0/user-data"))).split ","
log.info {"Topics:" + topics.to_s()}


log.info {"Sentimentizer Stats Monitor Started..."}

dynamo_db = AWS::DynamoDB.new

#The DynamoDB Tables wont be resized during a run, so get those
#table_list =Array.new
#table_list = ["SentimentizerTopicTotals", "SentimentizerTopicStats", "SentimentizerMetaData", "SentimentizerCost"]
#dynamo_db_read_rate = 0.0074 #set for Sydney region
#dynamo_db_write_rate = 0.0074 #set for region

#table_list.each do|a|
#	dynamo_db_read_total = dynamo_db_read_total + dynamo_db.tables[a].read_capacity_units
#	dynamo_db_write_total = dynamo_db_write_total + dynamo_db.tables[a].write_capacity_units
#end

#Get DynamoDB table throughput figures and multiply by price
#Then create hourly rate

#dynamo_db_hourly_rate = ((dynamo_db_read_total / 50) * dynamo_db_read_rate) + ((dynamo_db_write_total / 10) * dynamo_db_write_rate) 

#Get the cost of this Stats Micro
micro_hourly_rate = 0.02 #Sydney Region Rate


#Infinite Loop
loop {
	start_total = Array.new
	finish_total = Array.new

	#Get the current size of the worker ASG

	#Multiply the instance count by the instance size price

	#Divide into the hourly rate

	#Get the number of shards
	#Calculate the hourly rate


	totals_table = dynamo_db.tables['SentimentizerTopicTotals']
	totals_table.load_schema


	#get start seconds so we do not wait too long between calls

	start_time = Time.now
	counter = 1
	topics.each do|i| 

		#need to run through 20 times for each partitioned key
		for counter in 1..20

			topic_key = i.to_s() + "." + counter.to_s()

			#log.info{"i:" + i}
			#Read Totals Table
			item = totals_table.items.at(topic_key)
			log.info(topic_key)

			if item.exists? == false
				log.info {"Invalid Topic: " + i.to_s()}
				break
			end
			#log.info{"item" + item.attributes.to_h.inspect}
			if start_total[counter] == nil
				start_total[counter] = 0
			end


			start_total[counter] = start_total[counter] + item.attributes['sentimentVotes']

		end
		
		counter += 1	
	end

	end_time = Time.now
	wait_seconds = 1

	#Wait 60 seconds
	log.info {"Wait:" + wait_seconds.to_s}
	sleep (wait_seconds)

	counter = 1
	topics.each do|i|

		#need to run through 20 times for each partitioned key
		for counter in 1..20

			topic_key = i.to_s() + "." + counter.to_s()

			#log.info{"i:" + i}
			#Read Totals Table
			item = totals_table.items.at(topic_key)
			#log.info(topic_key)

			if item.exists? == false
				log.info {"Invalid Topic: " + i.to_s()}
				break
			end
			#log.info{"item" + item.attributes.to_h.inspect}
			if finish_total[counter] == nil
				finish_total[counter] = 0
			end


			finish_total[counter] = finish_total[counter] + item.attributes['sentimentVotes']

		end
		
		votes_per_second = 0
		
		#subtract totals
		for x in 1..20
			votes_per_second = votes_per_second + (finish_total[x] - start_total[x])
		end
		log.info { i.to_s() + " Votes per second:" + votes_per_second.to_s("F")}
		
		#write to data tabel
		stats_table = dynamo_db.tables['SentimentizerTopicStats']
		stats_table.load_schema

		item = stats_table.items.put(:topic => i.to_s())
		item.attributes.set(:votesPerSecond => votes_per_second)

  		counter += 1 
	end

	#Add all the total votes and convert to kinesis PUT cost 
	#Tally everything into a Total Cost table
	#Dynamo+micro+ASG+Shards+kinesis puts

	#Table is ]SentimentizerCost
	#Hash: Dimension (DynamoDB, Monitor, WorkerASG, KinesisShards, Kinesis Puts)
	#data field: HourlyCost (Number)



}


