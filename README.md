# ReadMe

Ruby server-side program to create statistics for each specified topic. 

# How to use
1. Install on a serve with ruby (I have used a bitnami image to save install time - remember the login is bitnami@ rather than ec2-user@)
2. Setup to run automatically at reboot. This is done my having the following line in your rc.local file
```
su -c "/home/bitnami/stack/ruby/bin/ruby /home/bitnami/sentimentiser-stats.rb > stats.out" - bitnami &
```
3. Ensure your EC2 user-data is populated with a comma-delimited set of topics to monitor e.g.:

```
TopicA,TopicB,TopicC
```